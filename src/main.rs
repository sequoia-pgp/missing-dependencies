use anyhow::anyhow;
use cargo_metadata::{CargoOpt, DependencyKind, Metadata, MetadataCommand};
use clap::Parser;
use log::{debug, error, info, trace};
use semver::{Version, VersionReq};
use std::collections::HashMap;
use std::fs::read;
use std::path::{Path, PathBuf};
use tabled::{Style, Table, Tabled};

fn main() {
    if let Err(e) = fallible_main() {
        error!("ERROR: {}", e);
        std::process::exit(1);
    }
}

fn fallible_main() -> anyhow::Result<()> {
    pretty_env_logger::init();
    let args = Args::parse();

    info!("load packaged crates");
    let packaged = Crates::from_file(&args.packaged)?;

    info!("load dependencies with version requirements");
    let metadata = load_cargo_metadata(&args)?;

    let dependencies = collect_dependencies(&metadata);
    info!("found {} dependencies", dependencies.len());

    info!("find dependencies that are not packaged");
    let problems = find_problems(&packaged, &dependencies);
    let count = problems.len();

    if count == 0 {
        info!("all good");
        Ok(())
    } else {
        report_problems(problems, &args.style);
        Err(anyhow!("there were {} missing dependencies", count))
    }
}

fn load_cargo_metadata(args: &Args) -> anyhow::Result<Metadata> {
    let mut metadata = MetadataCommand::new();
    metadata.other_options(vec!["--filter-platform=x86_64-unknown-linux-gnu".into()]);
    metadata.manifest_path(&args.dirname.join("Cargo.toml"));

    if args.all_features {
        metadata.features(CargoOpt::AllFeatures);
    } else if !args.features.is_empty() {
        metadata.features(CargoOpt::NoDefaultFeatures);
        metadata.features(CargoOpt::SomeFeatures(args.features.clone()));
    };

    info!("run cargo command: {:?}", metadata.cargo_command());
    Ok(metadata.exec()?)
}

fn collect_dependencies(metadata: &Metadata) -> HashMap<String, VersionReq> {
    let mut dependencies: HashMap<String, VersionReq> = HashMap::new();
    for package in metadata.packages.iter() {
        for dep in &package.dependencies {
            if dep.target.is_none()
                && matches!(dep.kind, DependencyKind::Build | DependencyKind::Normal)
            {
                trace!("package {} platform {:?}", package.name, dep.target);
                let name = canonicalize_crate_name(&dep.name);
                trace!("dependency on {} {}", name, dep.req);
                dependencies.insert(name, dep.req.clone());
            }
        }
    }
    dependencies
}

fn find_problems(packaged: &Crates, dependencies: &HashMap<String, VersionReq>) -> Vec<Problem> {
    let mut problems = Problems::default();
    for (name, req) in dependencies.iter() {
        debug!("consider {}, version {} required", name, req);
        if let Some(c) = packaged.get(name) {
            if !req.matches(&c.version) {
                info!(
                    "crate {} is packaged, but version doesn't satisfy {}",
                    c.orig_name, c.version
                );
                problems.push(Problem::MissingVersion(MissingVersion {
                    name: c.orig_name.clone(),
                    req: req.clone(),
                    version: c.version.clone(),
                }));
            } else {
                info!(
                    "crate {} is packaged and version satisfies {}",
                    c.orig_name, c.version
                );
            }
        } else {
            info!("crate {} is not packaged at all", name);
            problems.push(Problem::MissingCrate(MissingCrate {
                name: name.clone(),
                req: req.clone(),
            }));
        }
    }
    problems.to_vec()
}

fn report_problems(mut problems: Vec<Problem>, style: &str) {
    problems.sort_by_key(|p| match p {
        Problem::MissingCrate(x) => x.name.clone(),
        Problem::MissingVersion(x) => x.name.clone(),
    });

    let versions: Vec<&MissingVersion> = problems
        .iter()
        .filter_map(|p| match p {
            Problem::MissingVersion(x) => Some(x),
            _ => None,
        })
        .collect();

    let crates: Vec<&MissingCrate> = problems
        .iter()
        .filter_map(|p| match p {
            Problem::MissingCrate(x) => Some(x),
            _ => None,
        })
        .collect();

    let got_versions = !versions.is_empty();
    if got_versions {

        let table = match style {
            "markdown" => Table::new(versions).with(Style::markdown()),
            _ => Table::new(versions).with(Style::modern()),
        };
        println!("Table: Packaged version is not what is required");
        println!();
        println!("{}", table.to_string());
    }

    if !crates.is_empty() {
        if got_versions {
            println!();
        }
        let table = match style {
            "markdown" => Table::new(crates).with(Style::markdown()),
            _ => Table::new(crates).with(Style::modern()),
        };
        println!("Table: Required crate is not packaged at all");
        println!();
        println!("{}", table.to_string());
    }
}

/// List Rust dependeencies that haven't been been packaged in a target
/// operating system.
///
/// The list of dependencies needed is read from the standard input,
/// and should be in the form of output of "cargo tree". Add any flags
/// to "cargo tree" to select features you need.
#[derive(Parser)]
struct Args {
    /// Ask cargo for all features.
    #[clap(long)]
    all_features: bool,

    /// Ask cargo for specified features (use once per feature).
    #[clap(short, long)]
    features: Vec<String>,

    /// Style of tables to use for reporting problems.
    #[clap(long, possible_values = ["modern", "markdown"], default_value = "modern")]
    style: String,

    /// List of crates and versions packaged in the target operating system.
    packaged: PathBuf,

    /// Directory of crate to analyze.
    dirname: PathBuf,
}

struct Crate {
    canonical_name: String,
    orig_name: String,
    version: Version,
}

impl Crate {
    fn new(name: &str, version: Version) -> Self {
        trace!("found {} {}", name, version);
        Self {
            canonical_name: canonicalize_crate_name(name),
            orig_name: name.into(),
            version,
        }
    }
}

fn canonicalize_crate_name(name: &str) -> String {
    name.replace('-', "_")
}

#[derive(Default)]
struct Crates {
    crates: HashMap<String, Crate>,
}

impl Crates {
    fn from_file(filename: &Path) -> anyhow::Result<Self> {
        let data = read(filename)?;
        let text = String::from_utf8(data)?;
        let mut crates = Crates::default();

        for (lineno, line) in text.lines().enumerate() {
            trace!("parsing line {}: {:?}", lineno, line);
            let mut words = line.split(' ');
            if let Some(name) = words.next() {
                if let Some(version) = words.next() {
                    if words.next().is_none() {
                        let version = Version::parse(version)?;
                        let c = Crate::new(name, version);
                        crates.crates.insert(c.canonical_name.clone(), c);
                    } else {
                        return Err(anyhow!(
                            "too many words on line {}: {}",
                            lineno,
                            filename.display()
                        ));
                    }
                } else {
                    return Err(anyhow!(
                        "no version on line {}: {}",
                        lineno,
                        filename.display()
                    ));
                }
            } else {
                return Err(anyhow!(
                    "no name on line {}: {}",
                    lineno,
                    filename.display()
                ));
            }
        }

        Ok(crates)
    }

    fn get(&self, name: &str) -> Option<&Crate> {
        let name = canonicalize_crate_name(name);
        self.crates.get(&name)
    }
}

#[derive(Debug, Clone, Eq, PartialEq)]
enum Problem {
    MissingCrate(MissingCrate),
    MissingVersion(MissingVersion),
}

#[derive(Debug, Clone, Eq, PartialEq, Tabled)]
struct MissingCrate {
    #[tabled(rename = "Crate")]
    name: String,

    #[tabled(rename = "Required version")]
    req: VersionReq,
}

#[derive(Debug, Clone, Eq, PartialEq, Tabled)]
#[tabled(rename = "Crates for which a packaged version is too old")]
struct MissingVersion {
    #[tabled(rename = "Crate")]
    name: String,

    #[tabled(rename = "Required version")]
    req: VersionReq,

    #[tabled(rename = "Packaged version")]
    version: Version,
}

#[derive(Default, Debug)]
struct Problems {
    problems: Vec<Problem>,
}

impl Problems {
    fn push(&mut self, new: Problem) {
        if !self.problems.iter().any(|p| *p == new) {
            self.problems.push(new);
        }
    }

    fn to_vec(&self) -> Vec<Problem> {
        self.problems.clone()
    }
}
