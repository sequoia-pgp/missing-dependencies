# List dependencies that haven't been packaged

Summary: For a given Rust project (crate), list the Rust dependencies
(crates) that it depends on, that aren't packaged for a target
operating system.

## Description

The problem: a Rust project wants to be packaged in various operating
systems, and wants to manage its Rust dependencies to mainly be on
versions of crates that are already packaged. This would make
packaging is less work.

This tool takes a list of packaged crates and versions and compares a
Rust crate's dependencies accordingly.

Operating systems such as Debian package dependencies separately, and
frown upon embedded copies of them, or on downloading dependencies at
build time. Without going into why they do that, it's something
projects using Rust have to live with, if they want their software
packaged for such systems.

Other operating systems don't package dependencies separately. For
those, a Rust program will just use dependencies from crates.io
directly.

This repository contains tooling to make it easier to see what Rust
dependencies (crates) are missing from a target operating system.
There are two parts:

* `debian-crate-packages` is a simple shell script that fetches the
  package list for the Debian "unstable" version, and produces a list
  of Rust crates and versions that have been packaged.

* The Rust program in this crate reads the outputs of the script and
  produces a list of missing crates or versions of some Rust project.

A crate may be entirely missing from the target system, or the target
may have package of an older version. The Rust program looks for both
problems.

The list of missing crates can be used to make informed decisions
about dependencies:

* should Rust project drop the dependency, or ask the target system to
  package it?
* should the Rust project lower the version of the dependency or ask
  the target system to update their package to a newer version?


## Example use

To be run from the source tree of the `missing-dependencies` crate:

```sh
$ ./debian-crate-packages > crates-in-debian
$ cargo run -- crates-in-debian ~/my/rust/project
Table: Packaged version is not what is required

┌───────────────────────┬──────────────────┬──────────────────┐
│ Crate                 │ Required version │ Packaged version │
├───────────────────────┼──────────────────┼──────────────────┤
│ blobby                │ ^0.1             │ 0.3.1            │
├───────────────────────┼──────────────────┼──────────────────┤
│ block-buffer          │ ^0.7             │ 0.10.2           │
├───────────────────────┼──────────────────┼──────────────────┤
│ block-padding         │ ^0.1             │ 0.2.1            │
├───────────────────────┼──────────────────┼──────────────────┤
│ clap-derive           │ =3.2.17          │ 3.2.15           │
├───────────────────────┼──────────────────┼──────────────────┤
│ deunicode             │ ^0.4.0           │ 1.3.1            │
├───────────────────────┼──────────────────┼──────────────────┤
│ digest                │ ^0.8             │ 0.10.3           │
├───────────────────────┼──────────────────┼──────────────────┤
│ generic-array         │ ^0.12            │ 0.14.4           │
├───────────────────────┼──────────────────┼──────────────────┤
│ hashbrown             │ ^0.8.1           │ 0.12.1           │
├───────────────────────┼──────────────────┼──────────────────┤
│ hyphenation           │ ^0.8.4           │ 0.7.1            │
├───────────────────────┼──────────────────┼──────────────────┤
│ itertools             │ ^0.8             │ 0.10.3           │
├───────────────────────┼──────────────────┼──────────────────┤
│ opaque-debug          │ ^0.2             │ 0.3.0            │
├───────────────────────┼──────────────────┼──────────────────┤
│ proc-macro-error-attr │ =1.0.4           │ 1.0.3            │
├───────────────────────┼──────────────────┼──────────────────┤
│ quick-error           │ ^1.0.0           │ 2.0.1            │
├───────────────────────┼──────────────────┼──────────────────┤
│ remove-dir-all        │ ^0.5             │ 0.7.0            │
├───────────────────────┼──────────────────┼──────────────────┤
│ serde-derive          │ =1.0.136         │ 1.0.142          │
├───────────────────────┼──────────────────┼──────────────────┤
│ sha1-asm              │ ^0.4             │ 0.5.1            │
├───────────────────────┼──────────────────┼──────────────────┤
│ strsim                │ ^0.10            │ 0.9.3            │
├───────────────────────┼──────────────────┼──────────────────┤
│ textwrap              │ ^0.14            │ 0.15.0           │
├───────────────────────┼──────────────────┼──────────────────┤
│ value-bag             │ =1.0.0-alpha.8   │ 1.0.0-alpha.9    │
└───────────────────────┴──────────────────┴──────────────────┘

Table: Required crate is not packaged at all

┌───────────────────────────┬──────────────────┐
│ Crate                     │ Required version │
├───────────────────────────┼──────────────────┤
│ atomic_polyfill           │ ^1               │
├───────────────────────────┼──────────────────┤
│ clippy                    │ 0.*              │
├───────────────────────────┼──────────────────┤
│ directories_next          │ ^2.0.0           │
├───────────────────────────┼──────────────────┤
│ fehler                    │ ^1               │
├───────────────────────────┼──────────────────┤
│ fehler_macros             │ ^1.0.0           │
├───────────────────────────┼──────────────────┤
│ git_testament             │ ^0.2             │
├───────────────────────────┼──────────────────┤
│ git_testament_derive      │ ^0.1.13          │
├───────────────────────────┼──────────────────┤
│ ndarray                   │ ^0.14            │
├───────────────────────────┼──────────────────┤
│ no_std_compat             │ ^0.4             │
├───────────────────────────┼──────────────────┤
│ packed_simd_2             │ ^0.3.7           │
├───────────────────────────┼──────────────────┤
│ pandoc                    │ ^0.8.0           │
├───────────────────────────┼──────────────────┤
│ pandoc_ast                │ ^0.8             │
├───────────────────────────┼──────────────────┤
│ pikchr                    │ ^0.1             │
├───────────────────────────┼──────────────────┤
│ roadmap                   │ ^0.4.4           │
├───────────────────────────┼──────────────────┤
│ rustc_rayon               │ ^0.3             │
├───────────────────────────┼──────────────────┤
│ rustc_std_workspace_alloc │ ^1.0.0           │
├───────────────────────────┼──────────────────┤
│ scale_info                │ ^1.0             │
├───────────────────────────┼──────────────────┤
│ serde_aux                 │ ^3.0             │
├───────────────────────────┼──────────────────┤
│ state                     │ ^0.5             │
├───────────────────────────┼──────────────────┤
│ subplot                   │ ^0.4.3           │
├───────────────────────────┼──────────────────┤
│ subplot_build             │ ^0.4.0           │
├───────────────────────────┼──────────────────┤
│ subplotlib                │ ^0.4.1           │
├───────────────────────────┼──────────────────┤
│ subplotlib_derive         │ ^0.4.0           │
├───────────────────────────┼──────────────────┤
│ sval                      │ =1.0.0-alpha.5   │
├───────────────────────────┼──────────────────┤
│ tempfile_fast             │ ^0.3.1           │
└───────────────────────────┴──────────────────┘
 ERROR missing_dependencies > ERROR: there were 44 missing dependencies
```

Set `RUST_LOG` to `debug` or `info` to get more detailed log messages.


## FAQ

### Why is only Debian supported?

That's the only thing the author uses. Patches to add support for
other operating systems would be greatly appreciated! The best way to
do that is to write a script, similar to `debian-crate-packages`, that
produces a list of names of crates and their versions that are
packaged for an operating system.

The format of the output is one line per crate, with name, a space,
and the version, and nothing else.

The name of the crate should be the name as used by the crate itself.

### Why is only Debian unstable supported?

If Debian needs to package an entirely new crate, it'll be uploaded to
unstable. Likewise new versions of existing crates, except in serious
circumstances, mostly involving security problems. Thus, if a Rust
project wants to consider getting packaged for Debian, it'll need to
target unstable itself.

However, if you have different use case, by all means get in touch and
let's talk about it.
